{-# LANGUAGE AllowAmbiguousTypes  #-}
{-# LANGUAGE UndecidableInstances #-}

module Seeder where

import Database.Persist.Postgresql
import Data.Tuple (swap)

import Config
import Deps
import Models

austin's_id :: Key Account
austin's_id = toSqlKey 1
austin :: Account
austin = Account "Austin Erlandson" "austin@erlandson.com" "secret"

emily's_id :: Key Account
emily's_id = toSqlKey 2
emily :: Account
emily = Account "Emily Kroll" "krollemily@ymail.com" "secret"

austinTodos :: [Todo]
austinTodos =
    Todo austin's_id <$> ["All one", "all two", "All three", "all four", "All five"]

emilyTodos :: [Todo]
emilyTodos =
    Todo emily's_id <$> ["Any one", "any two", "Any three", "Any four", "Any five"]

-- date :: String -> UTCTime
-- date str = zonedTimeToUTC . read $ str ++ ":00 CDT"

main :: IO ()
main = do
    putStrLn "Migrating and Seeding DB"
    loadConf >>= mkPool >>= runSqlPool seed

seed :: SqlPersistT IO ()
seed = do
    printMigration migrateAll
    runMigration migrateAll

    repsertMany [(austin's_id, austin), (emily's_id, emily)]
    repsertMany . keyed $ austinTodos
    repsertMany . keyed $ emilyTodos

keyed :: [a] -> [(Key Todo, a)]
keyed = fmap (swap . fmap toSqlKey . swap) . indexed


indexed :: [a] -> [(Int64, a)]
indexed = go 1
  where
    go i (a:as) = (i, a) : go (i + 1) as
    go _ _ = []
