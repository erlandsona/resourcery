module Server where

-- Major imports exposing everything
-- import Servant.Server.Generic
import Web.Scotty

import Network.Wai (Middleware)
-- import Network.Wai.Handler.Warp (run)
import Network.Wai.Middleware.Cors (CorsResourcePolicy(..), cors)
import Network.Wai.Middleware.Gzip (def, gzip)
import Network.Wai.Middleware.RequestLogger (logStdout, logStdoutDev)


import API (api)

-- Local Imports
-- import Deps ((&))
import Deps ((#), liftIO)
import Config

-- Main
main :: IO ()
main = do
    conf@ServerConfig { env, port } <- loadConf
    pool                            <- mkPool conf
    scotty port $ do
        middleware corsified
        middleware compression
        middleware $ case env of
            Localhost  -> logStdoutDev
            Production -> logStdout

        -- Routes
        get "/" $ file "assets/index.html"
        matchAny "/" $ do
            setHeader "Content-Type" "application/json; charset=utf-8"
            raw =<< (body >>= api pool # liftIO)

-- | @x-csrf-token@ allowance.
-- The following header will be set: @Access-Control-Allow-Headers: x-csrf-token@.
-- allowCsrf :: Middleware
-- allowCsrf = addHeaders [("Access-Control-Allow-Headers", "x-csrf-token,authorization")]

-- | CORS middleware configured with 'appCorsResourcePolicy'.
corsified :: Middleware
corsified = cors (const $ Just appCorsResourcePolicy)
  where
        -- | Cors resource policy to be used with 'corsified' middleware.
        --
        -- This policy will set the following:
        --
        -- * RequestHeaders: @Content-Type, Authorization, Origin@
        -- * MethodsAllowed: @OPTIONS, GET, PUT, POST@
    appCorsResourcePolicy :: CorsResourcePolicy
    appCorsResourcePolicy = CorsResourcePolicy
        { corsOrigins        = Nothing
        , corsMethods        = ["OPTIONS", "GET", "PUT", "POST"]
        , corsRequestHeaders = ["Authorization", "Content-Type", "Origin"]
        , corsExposedHeaders = Nothing
        , corsMaxAge         = Nothing
        , corsVaryOrigin     = False
        , corsRequireOrigin  = False
        , corsIgnoreFailures = False
        }

compression :: Middleware
compression = gzip def
