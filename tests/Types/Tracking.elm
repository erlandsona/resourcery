module Types.Tracking exposing (suite)

import AssocList as Dict
import Expect
import Helpers.Date exposing (cal)
import Spell
import Test exposing (..)
import Time exposing (Month(..))
import Types.Data exposing (..)


suite : Test
suite =
    describe "Spell.cast"
        [ test "adds mastery from incantation to sourcerers prowess" <|
            \() ->
                let
                    { skills } =
                        Spell.cast
                            (cal 2018 Oct 23)
                            domInfo
                            incantation1Info
                            magic
                in
                skills
                    |> Dict.toList
                    |> Expect.equalLists
                        [ ( php, 33 )
                        , ( ruby, 10 )
                        , ( elm, 13 )
                        ]
        , test "doesn't add duplicate skills from mastery to sourcerers prowess" <|
            \() ->
                let
                    { skills } =
                        Spell.cast
                            (cal 2018 Oct 23)
                            domInfo
                            incantation2Info
                            magic
                in
                skills
                    |> Dict.toList
                    |> Expect.equalLists
                        [ ( php, 46 )
                        , ( ruby, 10 )

                        -- given oop is php's parent
                        -- adding the skill but no points
                        -- because oop has 5+ children...
                        , ( oop, 2 )
                        ]
        , test "Adds points / 2 ^ number of direct descendants of skill in graph." <|
            \() ->
                let
                    { skills } =
                        Spell.cast
                            (cal 2018 Oct 23)
                            domInfo
                            incantation2Info
                            magic
                in
                skills
                    |> Dict.toList
                    |> Expect.equalLists
                        [ ( php, 46 )
                        , ( ruby, 10 )

                        -- given oop is php's parent
                        -- adding the skill but no points
                        -- because oop has 5+ children...
                        , ( oop, 2 )
                        ]
        ]
