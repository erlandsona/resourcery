module Types.Estimation exposing (suite)

import Basics exposing (Float(..))
import Estimate exposing (Scope(..))
import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer)
import Helpers.Date exposing (cal)
import Journey exposing (Caesura(..))
import Result exposing (Result(..))
import Result.Extra as ResultE
import Sourcerer
import Spell
import Symmathesy exposing (Symmathesy)
import Test exposing (..)
import Time exposing (Month(..))
import Types.Data exposing (..)


test_ s x =
    test s <| \() -> x


suite : Test
suite =
    describe "How long until what I'm asking for will become a reality?"
        [ describe "velocity"
            [ Sourcerer.velocity fourteenPointsPerJourney
                |> Expect.equal 14
                |> test_ "fourteenPointsPerJourned returns a weighted avg of a Past"
            , Sourcerer.velocity fifteenPointsPerJourney
                |> Expect.equal 15
                |> test_ "a tale with a couple Journeys@15 points per"
            , Sourcerer.velocity shortPast
                |> Expect.equal 1
                |> test_ "a tale with a single Journey@1 point"
            ]
        , describe "Estimate.in_"
            [ -- fuzz caesuraF "given summons not found returns Scope of 0" <|
              -- \caesura ->
              --     Estimate.in_ caesura "blah" middleEarth
              --         |> Expect.equal
              --             (Approximately 0 caesura)
              -- Can't provide an estimate for non-existant summons.
              -- , fuzz caesuraF "given a emptySummons (Dict.empty has no keys) returns Scope of 0" <|
              --     \caesura ->
              --         Estimate.in_ caesura "blah" { middleEarth | summons = emptySummons }
              --             |> Expect.equal
              --                 (Approximately 0 caesura)
              fuzz caesuraF "given a summonsNoQuestOneFellow returns Scope of 0" <|
                \caesura ->
                    Estimate.in_ caesura summonsNoQuestOneFellowId { middleEarth | summons = summonsNoQuestOneFellow }
                        |> Expect.equal
                            (Approximately 0 caesura)
            , fuzz caesuraF "given a shortSummonsNoFellows returns Infinity" <|
                \caesura ->
                    Estimate.in_ caesura shortSummonsNoFellowsId { middleEarth | summons = shortSummonsNoFellows }
                        |> Expect.equal (Approximately (round (1 / 0)) caesura)
            , Estimate.in_ Hours fiftyPointSummonsId { middleEarth | summons = fiftyPointSummons }
                |> Expect.equal (Approximately 293 Hours)
                |> test_ "given a fiftyPointSummons with austin returns in Hours"
            , Estimate.in_ Days fiftyPointSummonsId { middleEarth | summons = fiftyPointSummons }
                |> Expect.equal (Approximately 37 Days)
                |> test_ "given a fiftyPointSummons with austin returns in Days"
            , Estimate.in_ Weeks fiftyPointSummonsId { middleEarth | summons = fiftyPointSummons }
                |> Expect.equal (Approximately 7 Weeks)
                |> test_ "given a fiftyPointSummons with austin returns in Weeks"
            , Estimate.in_ Hours fiftyPointSummonsTwoFellowsId { middleEarth | summons = fiftyPointSummonsTwoFellows }
                |> Expect.equal (Approximately 152 Hours)
                |> test_ "given a fiftyPointSummons returns In Hours"
            , Estimate.in_ Days fiftyPointSummonsTwoFellowsId { middleEarth | summons = fiftyPointSummonsTwoFellows }
                |> Expect.equal (Approximately 19 Days)
                |> test_ "given a fiftyPointSummons returns In Days"
            , Estimate.in_ Weeks fiftyPointSummonsTwoFellowsId { middleEarth | summons = fiftyPointSummonsTwoFellows }
                |> Expect.equal (Approximately 4 Weeks)
                |> test_ "given a fiftyPointSummons returns In Weeks"
            ]
        ]


caesuraF : Fuzzer Caesura
caesuraF =
    Fuzz.oneOf <| List.map Fuzz.constant [ Hours, Days, Weeks ]
