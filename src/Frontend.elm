module Frontend exposing (Model, app)

-- import Json.Decode as D

import Browser.Dom as Dom
import Debug exposing (toString)
import Html exposing (Html, input, text)
import Html.Attributes exposing (autofocus, id, placeholder, style, type_, value)
import Html.Events exposing (keyCode, on, onClick, onInput)
import Json.Decode as D
import Lamdera.Frontend as Lamdera
import Lamdera.Types exposing (..)
import Task
import Types exposing (..)


{-| Lamdera applications define 'app' instead of 'main'.

Lamdera.Frontend.application is the same as Browser.application with the
additional update function; updateFromBackend.

-}
app =
    Lamdera.application
        { init = \_ _ -> init
        , update = update
        , updateFromBackend = updateFromBackend
        , view =
            \model ->
                { title = "Lamdera chat demo"
                , body = [ view model ]
                }
        , subscriptions = \m -> Sub.none
        , onUrlChange = \_ -> Noop
        , onUrlRequest = \_ -> Noop
        }


type alias Model =
    FrontendModel


init : ( Model, Cmd FrontendMsg )
init =
    -- When the app loads, we have no messages and our message field is blank.
    -- We send an initial message to the backend, letting it know we've joined,
    -- so it knows to send us history and new messages
    ( { messages = [], messageFieldContent = "" }, Lamdera.sendToBackend ClientJoin )


{-| This is the normal frontend update function. It handles all messages that can occur on the frontend.
-}
update : FrontendMsg -> Model -> ( Model, Cmd FrontendMsg )
update msg model =
    case msg of
        -- User has changed the contents of the message field
        MessageFieldChanged s ->
            ( { model | messageFieldContent = s }, Cmd.none )

        -- User has hit the Send button
        MessageSubmitted ->
            ( { model | messageFieldContent = "", messages = model.messages }
            , Cmd.batch
                [ Lamdera.sendToBackend (MsgSubmitted model.messageFieldContent)
                , focusOnMessageInputField
                , scrollChatToBottom
                ]
            )

        -- Empty msg that does no operations
        Noop ->
            ( model, Cmd.none )


{-| This is the added update function. It handles all messages that can arrive from the backend.
-}
updateFromBackend : ToFrontend -> Model -> ( Model, Cmd FrontendMsg )
updateFromBackend msg model =
    ( case msg of
        ClientJoinReceived clientId ->
            { model | messages = ClientJoined clientId :: model.messages }

        RoomMsgReceived ( clientId, text ) ->
            { model | messages = MsgReceived clientId text :: model.messages }

        ClientTimeoutReceived clientId ->
            { model | messages = ClientTimedOut clientId :: model.messages }
    , Cmd.batch [ scrollChatToBottom ]
    )


view : Model -> Html FrontendMsg
view model =
    Html.div (style "padding" "10px" :: fontStyles)
        [ model.messages
            |> List.reverse
            |> List.map viewMessage
            |> Html.div
                [ id "message-box"
                , style "height" "400px"
                , style "overflow" "auto"
                , style "margin-bottom" "15px"
                ]
        , chatInput model MessageFieldChanged
        , Html.button (onClick MessageSubmitted :: fontStyles) [ text "Send" ]
        ]


chatInput : Model -> (String -> FrontendMsg) -> Html FrontendMsg
chatInput model msg =
    input
        ([ id "message-input"
         , type_ "text"
         , onInput msg
         , onEnter MessageSubmitted
         , placeholder model.messageFieldContent
         , value model.messageFieldContent
         , style "width" "300px"
         , autofocus True
         ]
            ++ fontStyles
        )
        []


viewMessage : ChatMsg -> Html msg
viewMessage msg =
    case msg of
        ClientJoined clientId ->
            Html.div [ style "font-style" "italic" ] [ text <| clientId ++ " joined the chat" ]

        ClientTimedOut clientId ->
            Html.div [ style "font-style" "italic" ] [ text <| clientId ++ " left the chat" ]

        MsgReceived clientId message ->
            Html.div [] [ text <| "[" ++ clientId ++ "]: " ++ message ]

        ChatError wsErr ->
            let
                err =
                    case wsErr of
                        TimeoutWithoutEverBeingOnline ->
                            -- we didn't even try to send it, because we were offline
                            "it seems like we're offline"

                        SendAttempted ->
                            -- we tried to send, but don't know if it arrived
                            "we probably failed to send a message"

                        ReceivedAndProbablyProcessed ->
                            -- we know we reached Lamdera, but not if it reached the backend app
                            "we think the message was sent successfully, but we don't know for sure"

                        ReceivedButCertainlyNotProcessed ->
                            -- we got a nack; msg was rejected by lamdera before it reached the back
                            "the message wasn't received by the backend"
            in
            Html.div [ style "font-style" "italic" ] [ text err ]


fontStyles : List (Html.Attribute msg)
fontStyles =
    [ style "font-family" "Helvetica", style "font-size" "14px", style "line-height" "1.5" ]


scrollChatToBottom : Cmd FrontendMsg
scrollChatToBottom =
    Dom.getViewportOf "message-box"
        |> Task.andThen (\info -> Dom.setViewportOf "message-box" 0 info.scene.height)
        |> Task.attempt (\_ -> Noop)


focusOnMessageInputField : Cmd FrontendMsg
focusOnMessageInputField =
    Task.attempt (always Noop) (Dom.focus "message-input")


onEnter : FrontendMsg -> Html.Attribute FrontendMsg
onEnter msg =
    let
        isEnter code =
            if code == 13 then
                D.succeed msg

            else
                D.fail "not ENTER"
    in
    on "keydown" (keyCode |> D.andThen isEnter)


timeoutInMs =
    5 * 1000
