module Types exposing (..)

import Lamdera.Types exposing (ClientId, WsError)
import Set exposing (Set)


type alias FrontendModel =
    { messages : List ChatMsg, messageFieldContent : String }


type alias BackendModel =
    { messages : List Message, clients : Set ClientId }


type FrontendMsg
    = MessageFieldChanged String
    | MessageSubmitted
    | Noop


type ToBackend
    = ClientJoin
    | MsgSubmitted String


type BackendMsg
    = BNoop


type ToFrontend
    = ClientJoinReceived ClientId
    | ClientTimeoutReceived ClientId
    | RoomMsgReceived Message


type alias Message =
    ( String, String )


type ChatMsg
    = ClientJoined ClientId
    | ClientTimedOut ClientId
    | MsgReceived ClientId String
    | ChatError WsError
