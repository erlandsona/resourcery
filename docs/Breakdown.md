# Questions / Goals?

## For Everyone:
    - [] Illuminate the cost of context switching. Rolling w/ the Punches.
    - [] Who's available to work on what when? Forecasting.
    - [] How should we predict this Idea? Assisted Estimation
## For clients:
    - [] Where is my $ going? Budgeting
    - [x] How long until what I'm asking for will become a reality? Estimation
    - [] What are my options w/ resp. to solutions? Options

## For developers: This may be answered if we go with integrations...
    - [] What do I need to work on today based on what priorities?

## Q: Bugs should never enter a software system. But how?
- We didn't have time to test...
    - How can I build in time to test?

- Our deployment takes too long...
    - How can we build faster & simpler deployment systems?

## A:
The process of adding Ideas and turning them into Quests
creates a unique collaborative environment that enables
a value-based grouping mechanism for planning work.

## Q: What do change requests look like? Rolling w/ the punches.
## A:
In Resourcery there's a One-Directional flow through the system
to elliminate as much restriction to the flow of work as possible.
Proof of this idea can be seen in numerous different systems in industry
Such as:
    React, FRP, The Dev Ops Movement, Data Driven Design, Event Sourcing...

What this looks like is when new information is discovered by anyone
about the system it enters as an Idea gets formulated into an Incantation
and is then optionally applied to a Quest.

