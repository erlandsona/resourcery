{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE NoGeneralizedNewtypeDeriving #-}

module API (api) where

import Prelude hiding (id)
import qualified Data.ByteString.Lazy.Char8 as B
import Basement.IntegralConv (intToInt64)
import Data.Maybe (fromJust)
import Control.Monad.IO.Class (liftIO)
-- import Data.Either.Combinators (maybeToRight)
-- import Character (Deity(..))
import Control.Monad ((<=<))

-- import Config
import Deps
import Models


-- Can't derive Generic instance for Key :(
-- deriving instance (Typeable record, PersistEntity record) => GQLType (Entity record)
-- deriving instance (Typeable record, PersistEntity record) => GQLType (Key record)

data Query m = Query
  { account :: Id Account -> m Account
  , accounts :: m [Account]
  } deriving (Generic, GQLType)


api :: ConnectionPool -> B.ByteString -> IO B.ByteString
api pool = interpreter resolver
  where
    resolver :: GQLRootResolver IO () Query Undefined Undefined
    resolver = GQLRootResolver
        { queryResolver = Query { account = accountShow, accounts = accountIdx }
        , mutationResolver = Undefined
        , subscriptionResolver = Undefined
        }

    accountShow :: Id Account -> IORes e Account
    accountShow = lift . pure . fromJust <=< db . get . keyFromReqId

    accountIdx :: IORes e [Account]
    accountIdx = do
        entities <- db (selectList [] [])
        lift =<< entityVal <$> entities
        -- pure accounts

    db :: ReaderT SqlBackend IO record -> IORes e record
    db = lift . (`runSqlPool` pool)

-- data DeityArgs = DeityArgs
--   { name      :: Maybe Text -- Required Argument
--   , mythology :: Maybe Text -- Optional Argument
--   } deriving (Generic)

-- resolve :: Key Account -> IORes e Deity
-- resolve key =
--     liftEitherM $ dbDeity name mythology

-- rootResolver :: GQLRootResolver IO () Query Undefined Undefined
-- rootResolver = 

newtype Id a = Id { id :: Int }
    deriving (Generic)

keyFromReqId :: (ToBackendKey SqlBackend rec) => Id rec -> Key rec
keyFromReqId = toSqlKey . intToInt64 . id
