module MainT exposing (Data, Event(..))

import RemoteData exposing (WebData)
import Symmathesy exposing (Symmathesy)

type alias Data =
    WebData Symmathesy

type Event
    = NoOp
