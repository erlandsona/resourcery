module IndexT exposing (Data, Event(..))

import RouteT
import AuthT
import MainT

type Event
    = Route RouteT.Event
    | Auth AuthT.Event
    | Main MainT.Event

type alias Data =
    { route : RouteT.Data
    -- Pages
    , auth : AuthT.Data
    , mainD : MainT.Data
    }
