port module Port exposing
    ( Translation
    , jsToElm
    , elmToJs
    )

import Json.Decode as Dec
import Json.Encode as Enc


type alias Translation =
    { func : String, args : Enc.Value }


port elmToJs : Translation -> Cmd nothing


port jsToElm : (Dec.Value -> nothing) -> Sub nothing
