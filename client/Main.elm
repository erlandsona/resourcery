module Main exposing
    ( init
    , update
    )

import RemoteData exposing (RemoteData(..), WebData)
import Return exposing (Return)
import Symmathesy exposing (Symmathesy)
import MainT exposing (..)

update : Event -> Data -> Return Event (WebData Symmathesy)
update _ _ =
    Return.singleton NotAsked


init : Data
init =
    NotAsked
