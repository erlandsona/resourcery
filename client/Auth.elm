module Auth exposing
    ( init
    , subscriptions
    , update
    , view
    )

import AuthT exposing (..)
import Browser.Navigation exposing (Key)
import Element
    exposing
        ( Element
        , alignRight
        , fill
        , text
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import IndexT
import Json.Decode as Dec exposing (Decoder)
import Json.Encode as Enc
import Page
import Port
import Return exposing (Return, return)
import Route
import Style exposing (edges)


update : Key -> Event -> Data -> Return IndexT.Event Data
update key msg data =
    case msg of
        Changed cred ->
            (case cred of
                Username str ->
                    { data | username = str }

                Password str ->
                    { data | password = str }
            )
                |> Return.singleton

        Signed method ->
            { data | status = Loading }
                |> Return.singleton
                |> Return.effect_ (method |> aws)

        Then session ->
            { data | status = Recieved session }
                |> Return.singleton
                |> Return.command (Route.goto Page.Main key)


aws : Method -> Data -> Cmd nothing
aws fn { username, password } =
    let
        auth ( name, args ) =
            { func = name
            , args = Enc.list Enc.string args
            }
    in
    (case fn of
        In ->
            ( "signIn", [ username, password ] )

        Up ->
            ( "signUp", [ username, password ] )

        Out ->
            ( "signOut", [] )
    )
        |> auth
        |> Port.elmToJs


init : Data
init =
    { username = "", password = "", status = Dormant }


view : Page.Gate -> Data -> Element Event
view gate ({ username, password, status } as creds) =
    let
        placeholderText =
            Just << Input.placeholder [] << text
    in
    Element.column
        [ Border.rounded 3
        , Element.centerX
        , Element.centerY
        ]
        [ -- Element.el [] (text <| Debug.toString page)
          Element.column
            [ Style.space Style.sm
            , Style.pad Style.sm
            , Font.color Style.black
            , Background.color Style.purple
            ]
            [ Input.username []
                { label = Input.labelHidden "Username"
                , onChange = Username >> Changed
                , placeholder = placeholderText "Username"
                , text = username
                }
            , Input.currentPassword []
                { label = Input.labelHidden "Password"
                , onChange = Password >> Changed
                , placeholder = placeholderText "Password"
                , show = False
                , text = password
                }
            ]
        , Input.button
            [ alignRight
            , Style.padWith { edges | t = Style.sm }
            ]
            { label =
                (case status of
                    Dormant ->
                        "Speak, friend, and enter."

                    Loading ->
                        "..."

                    Recieved _ ->
                        "Welcome! To platform 9 & 3/4's"
                 -- Recieved (_) ->
                 --     "Doh! Try Again."
                )
                    |> text
            , onPress =
                Just <|
                    Signed <|
                        case gate of
                            Page.Register ->
                                Up

                            Page.Welcome ->
                                In
            }
        ]


subscriptions : Request -> Sub Event
subscriptions status =
    case status of
        Dormant ->
            Sub.none

        Loading ->
            -- : (Session -> Event) -> Sub Event
            Port.jsToElm Then

        Recieved _ ->
            Sub.none


decoder : Decoder Session
decoder =
    Dec.field "token" Dec.string
        |> Dec.map Token
