module Page exposing
    ( Gate(..)
    , Page(..)
    )

-- Model
-- Update


type Page
    = Auth Gate
    | Main


type Gate
    = Register
    | Welcome
