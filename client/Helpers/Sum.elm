module Helpers.Sum exposing (dictWithSet, list)

import AssocList as Dict exposing (Dict)
import AssocSet as Set exposing (Set)


dictWithSet :
    (magicTable -> number -> k -> comparable -> Float)
    -> Dict k number
    -> Set comparable
    -> magicTable
    -> Float
dictWithSet combif dictInstance setInstance table =
    Dict.foldl
        (\k dv dacc ->
            Set.foldl (combif table dv k >> (+)) dacc setInstance
        )
        0
        dictInstance



-- dict : (k -> number -> Float) -> Dict k number -> Float
-- dict f =
--     Dict.foldl (\k -> f k >> (+)) 0
-- set : (comparable -> Float) -> Set comparable -> Float
-- set f =
--     Set.foldl (f >> (+)) 0


list : (a -> number) -> List a -> number
list f =
    List.foldl (f >> (+)) 0
