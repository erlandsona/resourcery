module Style exposing
    ( area
    , black
    , edges
    , lg
    , md
    , none
    , pad
    , padWith
    , purple
    , sm
    , space
    , white
    , yellow
    , yellowT
    )

import Element
    exposing
        ( Attribute
        , Color
        , Length
        , fill
        , height
        , paddingEach
        , rgb255
        , rgba255
        , spacing
        , width
        )
import Element.Background as Background
import Element.Font as Font


area : Length -> List (Attribute msg)
area a =
    List.map ((|>) a) [ width, height ]


type Spacing
    = Pad Int


none : Spacing
none =
    Pad 0


sm : Spacing
sm =
    Pad 13


md : Spacing
md =
    Pad 27


lg : Spacing
lg =
    Pad 53


space : Spacing -> Attribute msg
space (Pad int) =
    spacing int


pad : Spacing -> Attribute msg
pad px =
    padWith
        { t = px
        , r = px
        , b = px
        , l = px
        }


padWith : Edges -> Attribute msg
padWith { t, r, b, l } =
    let
        (Pad top) =
            t

        (Pad right) =
            r

        (Pad bottom) =
            b

        (Pad left) =
            l
    in
    paddingEach { top = top, right = right, bottom = bottom, left = left }


type alias Edges =
    { t : Spacing
    , r : Spacing
    , b : Spacing
    , l : Spacing
    }


edges : Edges
edges =
    { t = none
    , r = none
    , b = none
    , l = none
    }


black : Color
black =
    rgb255 39 39 39


white : Color
white =
    rgb255 234 234 234


yellowT : Color
yellowT =
    rgba255 255 204 51 0.7


yellow : Color
yellow =
    rgb255 255 204 51


purple : Color
purple =
    rgb255 115 23 170
