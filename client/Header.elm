module Header exposing (view)

import Assets.Logo as Logo
import Element exposing (..)
import Element.Font as Font
import Style exposing (edges)


view : Element any
view =
    row
        [ width fill
        , alignTop
        , Style.padWith
            { edges
                | t = Style.sm
                , r = Style.sm
            }
        ]
        [ Logo.resourcery [ alignRight ]
        ]
