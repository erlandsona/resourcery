module ID exposing
    ( ID
    , gen
    , new
    , nil
    , value
    )

import Random exposing (Generator, Seed)
import UUID exposing (UUID)


type ID = ID UUID


gen : Seed -> ID
gen =
    Random.step UUID.generator
        >> Tuple.first
        >> ID


new : String -> ID
new str =
    let
        (ID null) = nil
    in
        null
        |> UUID.childNamed str
        |> ID


nil : ID
nil =
    ID UUID.nil


value : ID -> String
value (ID id) =
    -- AKA: UUID.canonical
    UUID.toString id
