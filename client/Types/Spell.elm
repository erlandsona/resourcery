module Spell exposing (cast)

import Incantation exposing (Philosophy(..))
import Magic
import Sourcerer
import Time exposing (Posix)


cast : Posix -> Sourcerer.Info -> Incantation.Info -> Magic.Table -> Sourcerer.Info
cast timestamp sourcerer { effort } magic =
    let
        (GuessOf complexity mastery) =
            effort

        { skills, tale } =
            sourcerer
    in
    { sourcerer
        | tale = ( complexity, timestamp ) :: tale
        , skills = Magic.learn skills mastery complexity magic
    }
