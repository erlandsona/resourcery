-- Forget Velocity , Let's Talk Acceleration . Jessica Kerr


module Symmathesy exposing (Symmathesy, summon)

-- System of Mutual Learning

import AssocList as Dict exposing (Dict)
import AssocSet as Set exposing (Set)
import Helpers.Round as Round
import Helpers.Sum as Sum
import Incantation exposing (Philosophy(..), Quest)
import IntDict
import Magic exposing (Mastery(..), Prowess)
import Maybe.Extra as MaybE
import Name exposing (Name(..))
import Patron
import Sourcerer
import Summons


type alias Symmathesy =
    { incantations : Incantation.Table
    , magic : Magic.Table
    , patrons : Patron.Table
    , summons : Summons.Table
    , sourcerers : Sourcerer.Table
    }



{-

   Symmathesy.summon

   Takes the Quest as input
   And returns "Competent & Available Sourcerers" = s
   where s
      -> has prowess or interest in the subset of skills required to complete the Quest
      AND
      -> is currently or about to be without a Quest or whos Quest is smallest in scope.
-}


type alias Fit =
    String


summon : Quest -> Symmathesy -> List ( Sourcerer.Id, Fit )
summon quest { sourcerers, magic, incantations } =
    if Set.isEmpty quest then
        []

    else
        let
            requested : Set Magic.Id
            requested =
                challenge quest incantations

            -- Set required skills for a project.
            -- "sales"
            scored : List ( Sourcerer.Id, Float )
            scored =
                sourcerers
                    |> Dict.map
                        (\_ { skills } ->
                            magic |> Magic.score skills requested
                        )
                    |> Dict.toList
                    |> List.sortBy Tuple.second
                    |> List.reverse
                    |> List.take 3

            denominator =
                Sum.list Tuple.second scored

            score2Percent : Float -> Fit
            score2Percent weight =
                Round.commercial 0 (weight / denominator * 100)
        in
        List.map (\( id, weight ) -> ( id, score2Percent weight )) scored


challenge : Quest -> Incantation.Table -> Set Magic.Id
challenge q =
    Dict.filter (\id _ -> Set.member id q)
        >> Dict.foldl
            (\_ { effort } ->
                let
                    (GuessOf _ (MasterOf requirements)) =
                        effort
                in
                Set.union requirements
            )
            Set.empty
