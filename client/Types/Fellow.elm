module Fellow exposing
    ( Ship
    , Spell(..)
    , Why(..)
    )

import AssocList as Dict exposing (Dict)
import ID exposing (ID)
import Incantation
import Set exposing (Set)
import Sourcerer


type alias Ship =
    Dict Sourcerer.Id (Dict Incantation.Id Spell)



-- What are the different possible states an Incantation could be in?


type Spell
    = Casting -- Nothing, Just (IncantationId <| Id 1)
    | Blocked Why
      -- Because "Waiting on Client / 3rd party?"
      -- Because "not well defined"
      -- Because "Code Review"
      -- etc...
    | Cast Thought


type Why
    = Because String


type alias Thought =
    -- Nothing, Skipped, This was harder than we thought?
    Maybe String
